# -*- coding: UTF-8 -*-
import time
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
#import unittest
#from django.http import HttpRequest
import time
import urllib2, json
import lxml.html as html

class RecipeScraper(object):

    def __init__(self):
        self.display = Display(visible=0, size=(800, 600))
        self.display.start()
        self.ingredients = {}
        self.base = "http://www.bbc.co.uk"
        self.setUp()
        self.lookup()
        self.tearDown()
        self.display.stop()

    def ingredient_search(self, url, ingredient):
        self.browser.get(url)
        title = self.browser.title
        input_box = self.browser.find_element_by_id('searchInput-large')
        input_box.send_keys(ingredient)
        url = "%s%s" % (self.base, ingredient)
        input_box.send_keys(Keys.ENTER)
        tags = self.browser.find_elements_by_xpath("//div[@class='left']/h3/a")
        print "Tags: "
        print tags
        #tags = self.get_all_tags(url, 'ul.resources li.with-image h4 a')
        for t in tags:
            url2 = t.get_attribute("href")
            print url2
            fname = "../data/recipe%s.html" % t.get_attribute("href").replace(self.base, "").replace("/", "-")
            fname = fname.replace("recipe-food-recipes-", "")
            #print fname
            html = self.get_site_html(url2)
            file = open(fname, "w")
            file.write(html)
            file.close()
            print "Write %s" % fname
            time.sleep(3)
        # searchInput-large
        #pass

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(7)

    def tearDown(self):
        self.browser.quit()

    def get_all_tags(self, url, tag):
        source = self.get_site_html(url)
        tree = html.document_fromstring(source)
        return tree.cssselect(tag)

    def get_site_html(self, url):
        source = urllib2.urlopen(url).read()
        return source

    def lookup(self):
        small_letters = map(chr, range(ord('a'), ord('z')+1))
        for l in small_letters:
            url = "%s/food/ingredients/by/letter/%s" % (self.base, l)
            tags = self.get_all_tags(url, 'ol.resources-by-letter li.resource a')
            self.ingredients[l] = []
            for t in tags:
                print t.get("href")
                self.ingredients[l].append(t.get("href"))
                self.ingredient_search("http://www.bbc.co.uk/food/recipes/", t.get("href").replace("/food/", "").replace("_", " "))
            print self.ingredients[l]
            self.harvest(self.ingredients[l])
            time.sleep(2)

    def harvest(self, letter_list):
        for l in letter_list:
            url = "%s%s" % (self.base, l)
            tags = self.get_all_tags(url, 'ul.resources li.with-image h4 a')
            for t in tags:
                url2 = "%s%s" % (self.base, t.get("href"))
                fname = "../data/recipe%s.html" % t.get("href").replace("/", "-")
                fname = fname.replace("recipe-food-recipes-", "")
                html = self.get_site_html(url2)
                file = open(fname, "w")
                file.write(html)
                file.close()
                print "Write %s" % fname
                time.sleep(3)

if __name__ == "__main__":
    rs = RecipeScraper()
