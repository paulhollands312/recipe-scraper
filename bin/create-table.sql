drop table if exists recipes.recipe;
            create table recipes.recipe
            (
            recipe_id integer primary key auto_increment,
	title text null,
	creator int null,
	programme int null,
	ingredients text null,
	method text null,
	html text null,
            insert_by integer null,
            insert_date bigint null,
            update_by integer null,
            update_date bigint null,
            expired_date bigint null
            );
