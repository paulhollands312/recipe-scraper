import psycopg2

class pgconnect(object):

    def query(self, sql, db, user, passwd):
        definition = "host='localhost' dbname='%s' user='%s' password='%s'" % (db, user, passwd)
        connection = psycopg2.connect(definition)
        cursor = connection.cursor()
        cursor.execute(sql)
        return cursor.fetchall()
