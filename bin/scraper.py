import time
import urllib2, json
#from bs4 import BeautifulSoup
import lxml.html as html

ingredients = {}

base = "http://www.bbc.co.uk"

def get_all_tags(url,tag):
    source = get_site_html(url)
    tree = html.document_fromstring(source)
    return tree.cssselect(tag)

def get_site_html(url):
    source = urllib2.urlopen(url).read()
    return source

def lookup():
    small_letters = map(chr, range(ord('a'), ord('z')+1))
    for l in small_letters:
        url = "%s/food/ingredients/by/letter/%s" % (base, l)
        tags = get_all_tags(url, 'ol.resources-by-letter li.resource a')
        ingredients[l] = []
        for t in tags:
            ingredients[l].append(t.get("href"))
        print ingredients[l]
        harvest(ingredients[l])
        time.sleep(2)

def harvest(letter_list):
    for l in letter_list:
        url = "%s%s" % (base, l)
        tags = get_all_tags(url, 'ul.resources li.with-image h4 a')
        for t in tags:
            url2 = "%s%s" % (base, t.get("href"))
            fname = "../data/recipe%s.html" % t.get("href").replace("/", "-")
            html = get_site_html(url2)
            file = open(fname, "w")
            file.write(html)
            file.close()
            print "Write %s" % fname
            time.sleep(3)


if __name__ == "__main__":
    lookup()
